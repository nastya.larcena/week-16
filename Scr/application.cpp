#include <iostream>
#include <iomanip>
#include <string>
#include <conio.h>
#include <ctime>

using namespace std;

class BusinessLogic {
public:
    void showMenu() {
        cout << endl << "\t\t\t\t  *** Main Menu ***" << endl;
        cout << "\t\t\t\t1. Withdrawal" << endl;
        cout << "\t\t\t\t2. Deposit" << endl;
        cout << "\t\t\t\t3. Check Balance" << endl;
        cout << "\t\t\t\t4. Funds Transfer" << endl;
        cout << "\t\t\t\t5. Exit" << endl;
        cout << "\t\t\t\t_________________________" << endl;
        cout << "\t\t\t\tEnter your choice: ";
    }

    int mainMenuSelection(int choice) {
        while (choice < 1 || choice > 5) {
            cout << endl << "\t\t\t\tPlease enter your choice (1, 2, 3, 4, or 5): ";
            cin >> choice;
        }
        return choice;
    }

    void welcomeScreen() {
        cout << "\t\t\t\tWelcome to the ATM!" << endl;
        cout << "\t\t\t\tPlease type your bank account number: ";
    }

    void receipt() {
        cout << endl << "\t\t\t\tReceipt:" << endl;
        // Print receipt details
    }

    void showLogo() {
        // Display logo
    }

    double enterAmountScreen(double money) {
        cout << "\t\t\t\tEnter the amount (php): ";
        cin >> money;
        return money;
    }

    void waiting(unsigned int milliseconds) {
        clock_t goal = milliseconds + clock();
        while (goal > clock());
    }
};

class Application {
private:
    BusinessLogic businessLogic;
public:
    void start() {
        int choice, account;
        string pin = "";
        char ast;

        cout << fixed << showpoint << setprecision(2);

        businessLogic.welcomeScreen();
        
        system("cls");
        businessLogic.showLogo();
        cout << "\n\n\t\t\t  Please type your bank account number: ";
        cin >> account;
        cout << "\n\t\t\t  Please type personal identification number (PIN): ";
        ast = _getch();
        
        while (ast != 13) {
            pin.push_back(ast);
            cout << '*';
            ast = _getch();
        }

        if (account != 221215 && pin != "221215") {
            cout << "\n\t\t\t  Invalid bank account and PIN. Please try again!" << endl;
            getch();
    		if (choice==1)
		{			
				system("cls");
				showLogo();
				cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 WITHDRAWAL TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
				cout << "\n\n\t\t\t [ 1 ] From Checking" << endl;
				cout << "\t\t\t [ 2 ] From Savings" << endl;
				cout << "\t\t\t [ 3 ] Quick Cash" << endl;
				cout << "\t\t\t [ 4 ] Back to Main Menu" << endl;
				cout << "\t\t\t _____________________________________________"<<endl;
				cout << "\t\t\t Enter your withdraw transaction: ";
				cin >> withdrawChoice;
					while (withdrawChoice <1 || withdrawChoice >4 )
						{
							system("cls");
							showLogo();
							cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 WITHDRAWAL TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
							cout << "\n\n\t\t\t Invalid Transaction!\n\t\t\t Please re-enter 1, 2, 3 or 4 :  ";
							cin >> withdrawChoice;
						}
					if (withdrawChoice == 4)
					{
						showMenu();
					}
					//Quick Cash
					else if (withdrawChoice == 3) 
					{
						//fn1353
						int amount1;
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 QUICK CASH TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t REMAINING BALANCE : Php 10000"<<endl;
						cout << "\n\t\t\t Enter the Amount (Php) : ";
						cin >> amount1;
						cout << "\n\t\t\t Your remaining quick cash balance: Php "<<10000 - amount1<<endl;
						waiting(3000);
						receipt();
					cout << "\n\t\t\t|TRANSACTION:\t\t    QUICK WITHDRAWAL  |"
					 	 << "\n\t\t\t|AMOUNT:\t\t    "<<amount1<<".00\t      |"
					 	 << "\n\t\t\t|CURRENT BAL:\t\t    "<<20000 - amount1<<".00\t      |"
						 << "\n\t\t\t|AVAILABLE BAL:\t\t    "<<20000 - amount1<<".00\t      |"
						 << "\n\t\t\t|\t\t\t\t\t      |\n\t\t\t|HUWAG IPAALAM ANG PIN SA IBA.\t\t      |"
						 << "\n\t\t\t|\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2|";
						waiting(10000);
						showMenu();
					}
					else if (withdrawChoice == 2)
					{
						int amount2;
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 CASH SAVINGS TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t AVAILABLE BALANCE : Php 10000" << endl;
						cout << "\t\t\t Enter the amount : ";
						cin >> amount2;
						cout << "\t\t\t Your remaining savings balance: Php "<<10000 - amount2<<endl;
						waiting(5000);
						system("cls");
						receipt();
					cout << "\n\t\t\t|TRANSACTION:\t\t    SAVINGS WITHDRAWAL|"
					 	 << "\n\t\t\t|AMOUNT:\t\t    "<<amount2<<".00\t      |"
					 	 << "\n\t\t\t|CURRENT BAL:\t\t    "<<20000 - amount2<<".00\t      |"
						 << "\n\t\t\t|AVAILABLE BAL:\t\t    "<<20000 - amount2<<".00\t      |"
						 << "\n\t\t\t|\t\t\t\t\t      |\n\t\t\t|HUWAG IPAALAM ANG PIN SA IBA.\t\t      |"
						 << "\n\t\t\t|\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2|";
						waiting(10000);
						//brought to you by code-projects.org
						showMenu();
					}
					else if (withdrawChoice == 1)
					{
						int amount3;
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 CASH CHECKING TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t AVAILABLE BALANCE : Php 20000" << endl;
						cout << "\t\t\t Enter the amount : ";
						cin >> amount3;
						cout << "\t\t\t Your remaining check balance: Php "<<20000 - amount3<<endl;
						waiting(3000);
						system("cls");
						receipt();
					cout << "\n\t\t\t|TRANSACTION:\t\t    CASH WITHDRAWAL   |"
					 	 << "\n\t\t\t|AMOUNT:\t\t    "<<amount3<<".00\t      |"
					 	 << "\n\t\t\t|CURRENT BAL:\t\t    "<<20000 - amount3<<".00\t      |"
						 << "\n\t\t\t|AVAILABLE BAL:\t\t    "<<20000 - amount3<<".00\t      |"
						 << "\n\t\t\t|\t\t\t\t\t      |\n\t\t\t|HUWAG IPAALAM ANG PIN SA IBA.\t\t      |"
						 << "\n\t\t\t|\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2|";
						waiting(10000);
						showMenu();							
					}
		}
		else if(choice==2)
		{
					system("cls");
					showLogo();
					cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 DEPOSIT TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
					cout << "\n\n\t\t\t [ 1 ] To Checking" << endl;
					cout << "\t\t\t [ 2 ] To Savings" << endl;
					cout << "\t\t\t [ 3 ] Back to Main Menu" << endl;
					cout << "\t\t\t _____________________________________________"<<endl;
					cout << "\t\t\t Enter your deposit transaction: ";
					cin >> depositChoice;
					while (depositChoice < 1 || depositChoice > 3)
						{
							system("cls");
							showLogo();
							cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 DEPOSIT TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
							cout << "\n\n\t\t\t Invalid Transaction!\n\t\t\t Please re-enter 1, 2 or 3 :  ";
							cin >> depositChoice;
						}
					if (depositChoice == 3)
					{
						showMenu();
					}
					else if (depositChoice == 2)
					{
						int depamount1;
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 SAVINGS DEPOSIT TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t Enter the deposit amount : Php ";
						cin >> depamount1;
						cout << "\t\t\t Your New Balance: Php "<<depamount1+10000<<endl;
						waiting(5000);
						system("cls");
						showMenu();						
		
					}
					else if (depositChoice == 1)
					{
						int depamount2;
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 CHECK DEPOSIT TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t Enter the deposit amount : Php ";
						cin >> depamount2;
						cout << "\t\t\t Your New Balance: Php "<<depamount2+20000<<endl;
						waiting(5000);
						system("cls");
						showMenu();						
					}					
		}
		else if (choice==3)
		{					
					system("cls");
					showLogo();
					cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 CHECK BALANCE TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
					cout << "\n\n\t\t\t [ 1 ] From Checking" << endl;
					cout << "\t\t\t [ 2 ] From Savings" << endl;
					cout << "\t\t\t [ 3 ] Back to Main Menu" << endl;
					cout << "\t\t\t _____________________________________________"<<endl;
					cout << "\t\t\t Enter Your Check Balance Choice: ";
					cin >> checkBalanceChoice;
						while (checkBalanceChoice < 1 || checkBalanceChoice > 3)
						{
							system("cls");
							showLogo();
							cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 CHECK BALANCE TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
							cout << "\n\n\t\t\t Invalid Transaction!\n\t\t\t Please re-enter 1, 2 or 3 :  ";
							cin >> checkBalanceChoice;
						}
					if (checkBalanceChoice == 3)
					{
						showMenu();
					}
					else if (checkBalanceChoice ==2)
					{
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 BALANCE SAVINGS TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t NEW AVAILABLE BALANCE : Php 10000";
						waiting(3000);
						showMenu();
					}
					else if (checkBalanceChoice ==1)
					{
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 BALANCE CHECK TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t NEW CHECK BALANCE : Php 20000";
						waiting(3000);
						showMenu();
					}
		}
		else if (choice==4)
		{	
					system("cls");
					showLogo();
					cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 FUND TRANSFER TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
					cout << "\n\n\t\t\t [ 1 ] Transfer from Savings to Checking" << endl;
					cout << "\t\t\t [ 2 ] Transfer from Checking to Savings" << endl;
					cout << "\t\t\t [ 3 ] Back to Main Menu" << endl;
					cout << "\t\t\t _____________________________________________"<<endl;
					cout << "\t\t\t Enter Your Funds Transfer Transaction: ";
					cin >> fundsTransferChoice;
					while (fundsTransferChoice < 1 || fundsTransferChoice > 3)
						{
							system("cls");
							showLogo();
							cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 FUND TRANSFER TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
							cout << "\n\n\t\t\t Invalid Transaction!\n\t\t\t Please re-enter 1, 2 or 3 :  ";
							cin >> fundsTransferChoice;
						}
					if (fundsTransferChoice == 3)
					{
						showMenu();
					}
					else if (fundsTransferChoice ==2)
					{
						int accntNo, checkNo, amntNo;
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 CHECK TRANSFER TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t NEW CHECK BALANCE : Php 20000";
						cout << "\n\t\t\t Account Number (for Transfer) : ";
						cin >> accntNo;
						cout << "\n\t\t\t Check Number : ";
						cin >> checkNo;
						cout << "\n\t\t\t Amount Transfer : Php ";
						cin >> amntNo;
						while (amntNo < 2000 || amntNo > 20000)
						{
							system("cls");
							showLogo();
							cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 CHECK TRANSFER TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2";
							cout << "\n\n\t\t\t Invalid Amount!\n\t\t\t Please try again!";
							cout << "\n\t\t\t Please enter Amount Transfer : Php ";
							cin >> amntNo;
							
						}
						waiting(1000);
						cout << "\n\n\t\t\t The amount of Php "<<amntNo<<" has been transfered to \n\t\t\t "<<accntNo<<" with a check # "<<checkNo<<endl;
						waiting(10000);
						showMenu();
					}
					else if (fundsTransferChoice ==1)
					{
						int accntNo, checkNo, amntNo;
						system("cls");
						showLogo();
						cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 SAVINGS TRANSFER TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2";
						cout << "\n\n\t\t\t NEW SAVINGS BALANCE : Php 10000";
						cout << "\n\t\t\t Account Number (for Transfer) : ";
						cin >> accntNo;
						cout << "\n\t\t\t Amount Transfer : Php ";
						cin >> amntNo;
						while (amntNo < 2000 || amntNo > 10000)
						{
							system("cls");
							showLogo();
							cout << endl <<" \t\t\t \xB2\xB2\xB2\xB2\xB2\xB2\xB2\xB2 SAVINGS TRANSFER TRANSACTION \xB2\xB2\xB2\xB2\xB2\xB2\xB2";
							cout << "\n\n\t\t\t Invalid Amount!\n\t\t\t Please try again!";
							cout << "\n\t\t\t Please enter Amount Transfer : Php ";
							cin >> amntNo;
							
						}
						waiting(1000);
						cout << "\n\n\t\t\t The amount of Php "<<amntNo<<" has been transfered to "<<accntNo;
						waiting(10000);
						showMenu();
					}
		}		
		else if (choice ==5)
		{
					cout << " \n\t\t\t Brought To You By code-projects.org" << endl << endl;
					getch();
		}
	} while (choice != 5);
}
	return 0;

class Interface {
public:
    static void main() {
        Application application;
        application.start();
    }
};

int main() {
    Interface::main();
    return 0;
}
