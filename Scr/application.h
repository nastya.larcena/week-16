#ifndef MATH_FUNCTIONS_H
#define MATH_FUNCTIONS_H

float square_area(float side);
float circle_area(float radius);

#endif 
