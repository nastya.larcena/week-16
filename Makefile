.RECIPEPREFIX = >

build: clean prepare
> g++ -o bin/project src/main.cpp

clean:
> rm -rf bin

prepare:
> mkdir bin

run:
> binbin/project src/main.cpp
